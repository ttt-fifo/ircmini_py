# IRC mini client using python

### Install

Put module ircmini.py somewhere in your sys.path.

### Example Usage

```python
from ircmini import IRC

NICK = 'mynick'

irc = IRC(server='127.0.0.1', port=6667)
# ---Here the connection to server is already established
irc.raw(f"USER {NICK} {NICK} {NICK} :ircmini")
irc.raw(f"NICK {NICK}")

irc.privmsg('mylovedone', 'Hello, loved one!')

# ---get_response(...) blocks until server answers
for answer in irc.get_response(filter_callback):
    print(answer)
    # ---in case our loved one answers, we break the loop
    if 'mylovedone' in answer:
        break

irc.raw('QUIT :bye')
```

* On object initialization the connection to server is established
* irc.privmsg(...) method sends a private message
* irc.raw(...) sends a raw command to the IRC server
* irc.get_response(...) is a generator, yielding server response lines
* NOTE: all PING requests from server are autoanswered with PONG. The PING is still returned by get_response(...) though.

### License

Author: Todor Tororov <ttodorov@null.net>

License: BSD 2 clause simplified. See LICENSE for details.
