#!/bin/env python3
"""
Unit tests for ircmini.py.

For these test to work you need fully functional IRC server listening at
server:port
"""
import unittest
from ircmini import IRC


# the recipient who will receive PRIVMS from the tests
recipient = 'XXX'
# IRC server and port
server = '127.0.0.1'
port = 6667


class IRCTest(unittest.TestCase):

    def test_00_login_logout(self):
        irc = IRC(server, port)
        irc.raw("USER ircmini ircmini ircmini :ircmini")
        irc.raw("NICK ircmini")
        irc.raw("QUIT :bye")

    def test_01_privmsg(self):
        irc = IRC(server, port)
        irc.raw("USER ircmini ircmini ircmini :ircmini")
        irc.raw("NICK ircmini")
        irc.privmsg(recipient, "Hello from ircmini.py")
        irc.raw("QUIT :bye")

    def test_02_privmsg_unicode(self):
        irc = IRC(server, port)
        irc.raw("USER ircmini ircmini ircmini :ircmini")
        irc.raw("NICK ircmini")
        irc.privmsg(recipient, "АдеИ Я Ж мнари")
        irc.raw("QUIT :bye")

    def test_03_get_response(self):
        irc = IRC(server, port)
        irc.raw("USER ircmini ircmini ircmini :ircmini")
        irc.raw("NICK ircmini")
        irc.privmsg(recipient, "Please write me back now")

        print()
        print('#')
        print(f"Waiting PRIVMSG from {recipient}")
        print('#')
        for answer in irc.get_response():
            print(answer)
            if recipient in answer:
                break
        irc.raw("QUIT :bye")


if __name__ == '__main__':
    unittest.main()
