#!/bin/env python3
"""Mini IRC client."""
import socket
import time
from collections import deque


class IRC:
    """Create irc client object."""

    # bytes to receive from server socket on one pass
    RECV_BUF_SIZE = 1024

    def __init__(self, server, port):
        """
        Initialize object.

        Opens connection to server

        server: the irc server hostname or ip address
        port: the irc server port

        object data:
        self.sock: the client socket, connected to server
        self.recvbuf: receive line buffer
        self.recvchunk: receive helper buffer
        """
        self.sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.sock.connect((server, port))
        self.recvbuf = deque()
        self.recvchunk = deque()

    def __del__(self):
        """
        Destruct object.

        Clean socket close
        """
        self.sock.shutdown(socket.SHUT_RDWR)
        time.sleep(0.2)
        self.sock.close()

    def catch_ping(self, str_data):
        """
        Catch PING requests from server.

        In case of server PING, responds with PONG

        str_data: string data received from server
        """
        if str_data.startswith('PING'):
            pong = list(str_data)
            # just change PING to PONG and leave rest untouched
            pong[1] = 'O'
            self.raw(''.join(pong))

    def privmsg(self, recipient, message):
        """
        Send PRIVMSG to a recipient on the IRC server.

        This is just a helper. Needed because most of the time the consumer
        of the object would like to send a message.

        recipient: the nickname of the recipient
        message: the text of the message (one liner)
        """
        self.raw(f"PRIVMSG {recipient} :{message}")

    def get_response(self):
        """
        Get a response from server.

        Receives server response lines, checks every line for PING, yields
        the response line.

        Generator. Yields the server response lines.
        """
        for response in self.recvline():
            self.catch_ping(response)
            yield response

    def recvline(self):
        r"""
        Receive line from IRC server and yield it.

        Ensures full line received from server socket, up until last '\n'.

        Generator. Yields the server response lines.
        """
        self.recvbuf.clear()

        while True:
            chunk_bytes = self.sock.recv(self.RECV_BUF_SIZE)
            if not chunk_bytes:
                raise RuntimeError('Server disconnect')
            chunk_str = chunk_bytes.decode(errors='ignore')
            self.recvchunk.clear()
            self.recvchunk.extend(chunk_str)

            while True:
                try:
                    character = self.recvchunk.popleft()
                except IndexError:
                    break

                if character == "\n":
                    response = ''.join(self.recvbuf)
                    self.recvbuf.clear()
                    yield response
                else:
                    self.recvbuf.append(character)

    def raw(self, raw_message):
        """
        Send raw IRC command to server.

        raw_message: string with raw IRC command
        """
        self.sock.sendall(f"{raw_message}\n".encode())
